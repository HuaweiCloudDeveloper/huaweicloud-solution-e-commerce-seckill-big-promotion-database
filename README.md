[TOC]

**解决方案介绍**
===============
该解决方案通过华为云[GaussDB(for Redis)](https://www.huaweicloud.com/product/gaussdbforredis.html)+[GaussDB(for MySQL)](https://www.huaweicloud.com/product/gaussdbformysql.html)，
适用于电商、物流等行业的运营部门对商品订单、物流信息等进行多维度查询和分析。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/e-commerce-seckill-big-promotion-database.html

**架构图**
---------------
![方案架构](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/e-commerce-seckill-big-promotion-database-min.png)

**架构描述**
---------------
该解决方案会部署如下资源：

- 创建三副本高可靠GaussDB(for Redis)实例，用于存储用户业务的商品、库存、物流等数据，完全兼容Redis，为用户提供高性能、低成本NoSQL数据库，同时保证秒杀期间的数据一致性。
- 创建主备高可靠的GaussDB(for MySQL)实例，用于存储用户业务的商品、库存、物流等数据，完全兼容MySQL，为用户提供高性能的关系型数据库。

**组织结构**
---------------

``` lua
huaweicloud-solution-E-commerce-seckill-big-promotion-database
├── e-commerce-seckill-big-promotion-database-standard.tf.json -- 资源编排模板
```
**开始使用**
---------------
**通过华为云Console使用：**

1.登录华为云GaussDB(for MySQL)服务控制台，查看实例管理列表。

图1 GaussDB (for MySQL)控制台

![GaussDB (for MySQL)控制台](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-001.png)



2.查看创建GaussDB(for MySQL)的读写内网地址和端口。

图2 GaussDB (for MySQL)读写内网地址和端口

![GaussDB (for MySQL)读写内网地址和端口](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-002.png)



3.下载GaussDB(for MySQL) SSL证书

图3 GaussDB (for MySQL) SSL证书下载

![GaussDB (for MySQL) SSL证书下载](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-003.png)



4.配置GaussDB (for MySQL)的内网安全组“Sys-default”规则，放通业务和GaussDB(for MySQL)间网络连接，添加安全组入方向规则，源地址为业务应用IP地址。

图4 GaussDB (for MySQL)内网安全组规则

![GaussDB (for MySQL)内网安全组规则](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-004.png)


图5 配置安全组规则

![配置安全组规则](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-005.png)




5.登录华为云GaussDB(for Redis)服务控制台，查看实例管理列表。

图6 GaussDB (for Redis)控制台

![图1 GaussDB (for Redis)控制台](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-006.png)

6.查看创建GaussDB(for Redis)的负载均衡地址和端口。

图7 GaussDB (for Redis)负载均衡地址和端口

![GaussDB (for Redis)负载均衡地址和端口](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-007.png)

7.配置GaussDB (for Redis)的内网安全组“Sys-default”规则，放通业务和GaussDB(for Redis)间网络连接，添加安全组入方向规则，源地址为业务应用IP地址。

图8 GaussDB (for Redis)内网安全组规则

![GaussDB (for Redis)内网安全组规则](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-008.png)

图9 配置安全组规则

![配置安全组规则](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/e-commerce-seckill-big-promotion-database/readme/readme-image-009.png)

8.在业务应用中配置GaussDB(for Redis)和GaussDB(for MySQL)的内网地址和端口及GaussDB(for MySQL)证书，用于业务连接数据库。

